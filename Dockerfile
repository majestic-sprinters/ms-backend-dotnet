FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
RUN apt update && apt install net-tools
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY LabraryApi/LabraryApi.csproj LabraryApi/
RUN dotnet restore "LabraryApi/LabraryApi.csproj"
COPY . .
WORKDIR "/src/LabraryApi"
RUN dotnet build "LabraryApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "LabraryApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "LabraryApi.dll"]